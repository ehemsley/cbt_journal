import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { MyApp } from './app.component';
import { Home } from '../pages/home/home';
import { Page2 } from '../pages/page2/page2';
import { NewEntry } from '../pages/new_entry/new_entry';
import { Entry } from '../pages/entry/entry';
import { Setup } from '../pages/setup/setup';
import { Authenticate } from '../pages/authenticate/authenticate';
import { PasswordService } from '../services/password.service';

import { MomentModule } from 'angular2-moment';

@NgModule({
  declarations: [
    MyApp,
    Home,
    Page2,
    NewEntry,
    Entry,
    Setup,
    Authenticate
  ],
  imports: [
    IonicModule.forRoot(MyApp,
    {
      platforms: {
        ios: {
          scrollAssist: false,
          autoFocusAssist: false
        }
      }
    }),
    MomentModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Home,
    Page2,
    NewEntry,
    Entry,
    Setup,
    Authenticate
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Storage,
    PasswordService
  ]
})
export class AppModule {}
