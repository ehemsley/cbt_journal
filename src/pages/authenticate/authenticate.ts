import {
  Component,
  trigger,
  state,
  style,
  transition,
  animate,
  keyframes
} from '@angular/core';

import { NavController } from 'ionic-angular';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Storage } from '@ionic/storage';

import { Keyboard } from 'ionic-native';

import { Home } from '../home/home';

import { PasswordService } from '../../services/password.service';

import sjcl from 'sjcl';

@Component({
  selector: 'page-authenticate',
  templateUrl: 'authenticate.html',
  animations: [
    trigger(
      'buttonShake', [
        transition('inactive => shaking', [
          animate('300ms', keyframes([
            style({transform: 'translateX(-10px)', backgroundColor: '#D81E5B', offset: 0.33}),
            style({transform: 'translateX(10px)', backgroundColor: '#D81E5B', offset: 0.66}),
            style({transform: 'translateX(0)', backgroundColor: '#D81E5B', offset: 1.0})
          ])
        )]),
        state('inactive', style({backgroundColor: '#387ef5'})),
        transition('shaking => inactive', animate('500ms ease-in')),
        state('authenticated',
          style({backgroundColor: '#03CEA4'})
        ),
        transition('inactive => authenticated', animate('500ms ease-in'))
      ]
    )
  ]
})

export class Authenticate {
  passwordForm: FormGroup;
  buttonState: string = 'inactive';

  constructor(private storage: Storage, builder: FormBuilder, private navCtrl: NavController, private passwordService: PasswordService) {
    this.passwordForm = builder.group({
      'password': []
    })
    Keyboard.disableScroll(true);
  }

  submitPassword() {
    this.storage.get('password').then((stored_hash) => {
      let hashed_pass = sjcl.hash.sha256.hash(this.passwordForm.controls['password'].value);

      if (sjcl.bitArray.equal(stored_hash, hashed_pass)) {
        this.buttonState = 'authenticated';
        Keyboard.close();
        this.passwordService.setPassword(this.passwordForm.controls['password'].value);
      } else {
        this.buttonState = 'shaking';
        this.passwordForm.controls['password'].setValue('');
      }
    });
  }

  shakeDone() {
    if (this.buttonState == 'shaking') {
      this.buttonState = 'inactive';
    } else if (this.buttonState == 'authenticated') {
      Keyboard.disableScroll(false);
      this.navCtrl.setRoot(Home, {}, {
        animate: true,
        direction: 'forward'
      });
    }
  }
}
