import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { Entry } from '../entry/entry';

import { PasswordService } from '../../services/password.service';

import sjcl from 'sjcl';

@Component({
  selector: 'page-page2',
  templateUrl: 'page2.html'
})
export class Page2 {
  selectedEntry: any;
  entries: Array<{date: Date, text: string}>;
  notEmpty: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, storage: Storage, private passwordService: PasswordService) {
    storage.get('entries').then((val) => {
      this.notEmpty = val != null && val.length > 0;
      if (this.notEmpty) {
        this.entries = [];
        for (let entry of val) {
          entry.date = new Date(entry.date); //massage the date
          entry.text = sjcl.decrypt(passwordService.Password(), entry.text);
          this.entries.push(entry);
        }
      }
    });
  }

  itemTapped(event, entry) {
    this.navCtrl.push(Entry, {
      entry: entry
    });
  }
}
