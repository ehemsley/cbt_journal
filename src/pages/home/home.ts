import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { NewEntry } from '../new_entry/new_entry';

import { Authenticate } from '../authenticate/authenticate';

import { Storage } from '@ionic/storage';

import { PasswordService } from '../../services/password.service';

@Component({
  selector: 'page-page1',
  templateUrl: 'home.html'
})

export class Home {
  mostRecentEntry: {date: Date, text: string}
  mostRecentEntryDate: Date;
  notEmpty: boolean;

  constructor(private navCtrl: NavController, private storage: Storage, private passwordService: PasswordService) {

  }

  ionViewDidEnter() {
    this.storage.get('entries').then((val) => {
      this.notEmpty = false;
      if (val != null && val.length > 0) {
        this.mostRecentEntry = val[0];
        this.mostRecentEntryDate = this.mostRecentEntry.date;
        this.notEmpty = true;
      }
    });
  }

  goToNewEntry() {
    this.navCtrl.setRoot(NewEntry, {}, {
      animate: true,
      direction: 'forward'
    });
  }

  lock() {
    this.passwordService.clearPassword();
    this.navCtrl.setRoot(Authenticate, {}, {
      animate: true,
      direction: 'back'
    });
  }
}
