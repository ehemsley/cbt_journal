import { Component } from '@angular/core';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { NavController, AlertController } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { Authenticate } from '../authenticate/authenticate';

import sjcl from 'sjcl';

@Component({
  selector: 'page-setup',
  templateUrl: 'setup.html'
})

export class Setup {
  loginForm: FormGroup;
  password: String;

  constructor(private navCtrl: NavController, builder: FormBuilder, private alertCtrl: AlertController, private storage: Storage) {
    this.loginForm = builder.group({
      'password': [
        '',
        Validators.compose([Validators.required, Validators.minLength(5)])
      ],
      'password-confirmation': [
        '',
        Validators.required
      ]
    }, {validator: this.matchingPasswords('password', 'password-confirmation')});
  }

  validate(): boolean {
    if (this.loginForm.valid) { return true; }

    let errorMessage = '';
    let passwordControl = this.loginForm.controls['password'];
    let confirmationControl = this.loginForm.controls['password-confirmation'];
    if (!passwordControl.valid) {
      if (passwordControl.errors['required']) {
        errorMessage = 'Password is required.';
      } else if (passwordControl.errors['minlength']) {
        errorMessage = 'Password must contain at least 5 characters.';
      }
    } else if (!confirmationControl.valid) {
      if (confirmationControl.errors['required']) {
        errorMessage = 'Password confirmation is required.';
      } else if (confirmationControl.errors['notEquivalent']) {
        errorMessage = 'Passwords do not match.';
      }
    }

    let alert = this.alertCtrl.create({
      title: 'Invalid Password',
      subTitle: errorMessage,
      buttons: ['OK']
    });

    alert.present();

    passwordControl.setValue('');
    confirmationControl.setValue('');

    return false;
  }

  submit() {
    if (this.validate()) {
      console.log('valid!!');
      let password = this.loginForm.controls['password'].value;

      this.storage.set('password', sjcl.hash.sha256.hash(password));
      this.storage.set('password_is_set', true);

      this.navCtrl.setRoot(Authenticate, {}, {
        animate: true,
        direction: 'forward'
      });
    }
  }

  matchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey];
      let passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({notEquivalent: true})
      }
    }
  }
}
