import { Injectable } from '@angular/core';

@Injectable()

export class PasswordService {
  private password: string;

  setPassword(givenPassword: string) {
    this.password = givenPassword;
  }

  Password(): string {
    return this.password;
  }

  clearPassword() {
    this.password = '';
  }
}
